# RAIDEN Readme
106062333 刁彥斌
## Scoring
### Basic
- Complete Game Process: 15% (Y)\
    Game starts from a **Menu**, where you can choose a stage and get into the game. The game runs forever; the longer you survive, more scores you may acquire.
- Basic Rules: 20% (Y)
    - Player: can shoot, move, and get hit by **both enemy units and their bullets**.
    - Enemy: 3 types of enemies are to be generated automatically. In each stage, the compositions of enemies are different.
    - Map: A 2-layered map that scrolls with different speed.
- Jucify Mechanisms: 15% (Y)
  - Level: Choose levels from the main menu.
  - Skill: Use **Ctrl** Key to cast the skill. It has a 10 second cooldown.
- Animations: 10% (Y)\
    There are animations on the player as player moves, as well as the enemies.
- Particle Systems: 10% (Y)\
    Particles are generated each time a bullet hits a unit, or another bullet.
- Sound Effect: 5% (Y)\
    Bullet hit sound, and unit explosion sound. Volume can be configured in the **Pause Menu**. (Press ESC to open it in game)
- UI: 5% (Y)\
    Player HP, Ultimate Skill CD, Score, Volume Control (in the pause menu), and pause menu are all done.
- Appearance: (5%)\
    Some self-made assets, 2 layer background, the SEKIRO death image remake, ...

### Bonus
- Enhanced Items: 15% (Y)
  - Player can get a 5-second auto-aiming buff, if he picks up a blue-arrow-buff-ball that appears randomly. (5%)
  - Player can get a 5-second special fireball buff, if he picks up a red-fire-buff-ball that appears randomly. The fireball can pierce through everything it hits, while dealing tons of damage to the enemy units. It cannot destroy the dragon's fire-breath though. (5%)
  - Player can get a 40% HP heal buff, if he picks up a red-cross-buff-ball that appears randomly. (5%)

### Those **NOT DONE**
Leaderboard, multiplayer, boss.

## Control
In the main menu, use mouse clicks to choose a stage.

In the game you have several operations:
- Arrow keys to move around
- Space bar to shoot
- Ctrl to cast an ultimate skill (10s CD)
- Esc to open pause menu

In the pause menu, click on the volume control icons to change volume.

Since hesitation is defeat, in the gameover scene, you can either use left-click to player again, or use right-click to go back to the main menu.

## Features
- There are 3 types of enemies:
  - The crows. They appear most frequently, deals secondly high damage, but has the lowest health. They pose a threat by number.
  - The bugs. They move the fastest, and shoot a lot. fortunately, they deal only a little damage.
  - The Mighty Dragons. The move very slow, shoots very less. However, their breath deals a great amount of damage.
- Bullets can cancel each other when they collide. However, the fire-breath from the mighty dragons are indestructable.
- Even in each stage, difficulty is gradually increasing as time goes on.