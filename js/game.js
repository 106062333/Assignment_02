// Increases difficulty every 12 seconds
var scrollSpeedGrow = [null, 0.1, 0.12, 0.14];
var scrollSpeedBase = [null, 0.1, 0.15, 0.2];
var scrollSpeedMax = [null, 1, 1.3, 1.7];

var spawnTimeDecay = [null, 100, 150, 200];
var spawnTimeBase = [null, 8000, 7000, 5500];
var spawnTimeMin = [null, 5500, 4500, 3000];

var monsterTypeProb = [
	null, 
	[0.7, 0.9, 1],
	[0.5, 0.8, 1],
	[0.45, 0.75, 1]
];
var monsterNumberMinMax = [
	null, 
	[1, 3],
	[2, 3],
	[2, 4]
];

var GameScore = 0;

//===========================
Game = {};

Game.preload = function() {
	game.load.spritesheet('player', 'img/player/ship_sheet.png', 87, 80);
	game.load.image('bul_pl_1', 'img/bullet/player1.png');
	game.load.image('bul_pl_s', 'img/bullet/special.png');
	game.load.image('spark', 'img/spark.png');
	game.load.spritesheet('e_crow', 'img/enemies/163-Small05.png', 40, 27, 4);
	game.load.spritesheet('e_bug', 'img/enemies/096-Monster10.png', 80, 64, 4);
	game.load.spritesheet('e_dragon', 'img/enemies/073-Bird03.png', 96, 96, 4);
	game.load.image('b1', 'img/stage1/bg.png');
	game.load.image('b2', 'img/stage1/l1.png');
	game.load.image('bul_e_crow', 'img/bullet/crow.png')
	game.load.image('bul_e_bug', 'img/bullet/bug.png')
	game.load.image('bul_e_dragon', 'img/bullet/dragon.png')
	game.load.audio('atk', 'sound/atk.ogg');
	game.load.audio('exp', 'sound/exp.ogg');
	game.load.spritesheet('explosion', 'img/explosion.png', 150, 150);
	game.load.image('pause', 'img/pause.png');
	game.load.image('hp', 'img/hp.png');
	game.load.image('hpbg', 'img/hpbg.png');
	game.load.image('buff1', 'img/buff_hp.png');
	game.load.image('buff2', 'img/buff_track.png');
	game.load.image('buff3', 'img/buff_fire.png');
}

Game.create = function() {
	// BUFFS
	this.bulletFollow = 0;
	//-----
	this.stage = 0; // Calculate difficulty
	this.scroll = scrollSpeedBase[selectedStage];
	this.nextEnemyAt = 0;
	this.spawnTime = spawnTimeBase[selectedStage];


	this.atke = Game.add.audio('atk');
	this.atkp = Game.add.audio('atk');
	this.exp = Game.add.audio('exp');

	this.bg1 = Game.add.sprite(0, -600, 'b1');
	this.bg2 = Game.add.sprite(0, 0, 'b1');
	this.ul1 = Game.add.sprite(0, -600, 'b2');
	this.bl1 = Game.add.sprite(0, 0, 'b2');

	this.tweens = [
	game.add.tween(this.bg1).to({y: 0}, 35000, Phaser.Easing.Linear.None, true, 0, 1000, false),
	game.add.tween(this.bg2).to({y: 600}, 35000, Phaser.Easing.Linear.None, true, 0, 1000, false),
	game.add.tween(this.ul1).to({y: 0}, 13500, Phaser.Easing.Linear.None, true, 0, 1000, false),
	game.add.tween(this.bl1).to({y: 600}, 13500, Phaser.Easing.Linear.None, true, 0, 1000, false)];
	Game.physics.startSystem(Phaser.Physics.ARCADE); 

	

	// Add player
	this.player = game.add.sprite(320, 400, 'player');
		this.player.anchor.setTo(0.5, 0.5);
		this.player.scale.setTo(0.7, 0.7);
		this.player.vel = 5;
		this.player.hp = 100;
	Game.physics.arcade.enable(this.player);
		this.player.body.drag.set(700);
		this.player.body.maxVelocity.set(300);
		this.player.body.collideWorldBounds = true;

	

	// Explosion Group
	/*this.explosions = Game.add.group();
		this.explosions.createMultiple(40, 'explosion');
		this.explosions.setAll('anchor.x', 0.5);
		this.explosions.setAll('anchor.y', 0.5);
		this.explosions.callAll('animations.add', 'animations', 'do', [0, 1, 2, 4, 5, 6, 9], 15, true, true);
		this.explosions.callAll('play', null, 'do');*/

	// Enemy group
	this.enemy = Game.add.group();
		this.enemy.createMultiple(50, 'e_crow');
		this.enemy.createMultiple(40, 'e_bug');
		this.enemy.createMultiple(30, 'e_dragon');
		//this.enemy.enableBody = true;
		this.enemy.setAll('anchor.x', 0.5);
		this.enemy.setAll('anchor.y', 0.5);
		this.enemy.forEach(function(e) {
			Game.physics.arcade.enable(e);
			e.hitByFireBall = null;
			if(e.key == 'e_crow')
			{
				e.body.setSize(27, 20, -27/2, -20/2);
				e.hp = 10;
				e.type = 'bullet';
				e.nextShootAt = 0;
				e.shootingDelay = 1000;
				e.phase = Game.rnd.frac() * Math.PI;
				e.coef = Game.rnd.frac() + 0.1;
				e.pos = Game.rnd.integerInRange(100, 700);
				e.range = Game.rnd.integerInRange(100, 100 * Math.exp(-(e.pos-400)*(e.pos-400)/40000) + 100);
				e.animations.add('fly', [0, 1, 2, 3], 4, true, true);
				e.play('fly');
			}
			else if(e.key == 'e_bug'){
				e.hp = 30;
				e.type = 'bullet';
				e.nextShootAt = 0;
				e.shootingDelay = 500;
				e.phase = Game.rnd.frac() * Math.PI;
				e.coef = Game.rnd.frac() * 1.2 + 0.1;
				e.pos = Game.rnd.integerInRange(300, 500);
				e.range = Game.rnd.integerInRange(100, 200 * Math.exp(-(e.pos-400)*(e.pos-400)/40000) + 150);
				e.animations.add('fly', [0, 1, 2, 3], 4, true, true);
				e.play('fly');
			}
			else if (e.key == 'e_dragon') {
				e.body.setSize(800, 96, 0, 0);
				e.hp = 120;
				e.type = 'bullet';
				e.nextShootAt = 0;
				e.shootingDelay = Game.rnd.integerInRange(2000, 3500);
				e.phase = Game.rnd.frac() * Math.PI;
				e.coef = Game.rnd.frac() * 0.2 + 0.1;
				e.pos = Game.rnd.integerInRange(350, 450);
				e.range = Game.rnd.integerInRange(100, 250 * Math.exp(-(e.pos-400)*(e.pos-400)/40000) + 100);
				e.animations.add('fly', [0, 1, 2, 3], 4, true, true);
				e.play('fly');
			}
			
			e.checkWorldBounds = true;
			e.outOfBoundsKill = true;			
		});



	// Player Bullet group
	this.playerBullet = Game.add.group();
		this.playerBullet.enableBody = true;
		this.playerBullet.createMultiple(70, 'bul_pl_1');
		this.playerBullet.createMultiple(30, 'bul_pl_s');
		this.playerBullet.setAll('anchor.x', 0.5);
		this.playerBullet.setAll('anchor.y', 0.5);
		this.playerBullet.forEach(function (e) {
			e.toFollow = null;
			e.checkWorldBounds = true;
			e.outOfBoundsKill = true;
		});

	this.bp = {delay:100};

	// Enemy Bullet Group
	this.enemyBullet = Game.add.group();
		this.enemyBullet.enableBody = true;
		this.enemyBullet.createMultiple(100, 'bul_e_crow');
		this.enemyBullet.createMultiple(200, 'bul_e_bug');
		this.enemyBullet.createMultiple(40, 'bul_e_dragon');
		this.enemyBullet.setAll('anchor.x', 0.5);
		this.enemyBullet.setAll('anchor.y', 0.5);
		this.enemyBullet.forEach(function (e) {
			e.checkWorldBounds = true;
			e.outOfBoundsKill = true;
		});


	this.nowTime = 0;
	// TODO: SUB BULLETS

	this.nextShootAt = 0;
	this.shootingDelay = 400;

	
	// Particle Emitter
	this.emitter = game.add.emitter(0, 0, 300);
	this.emitter.makeParticles('spark');
	this.emitter.setScale(1, 0, 1, 0, 500, Phaser.Easing.Linear.None, false);
	this.emitter.setXSpeed(-250, 250);
	this.emitter.setYSpeed(-250, 250);

	// Fire Button
	this.fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
	this.skillButton = this.input.keyboard.addKey(Phaser.KeyCode.CONTROL);
	this.escButton = this.input.keyboard.addKey(Phaser.KeyCode.ESC);
	this.escButton.onDown.add(this.changePause, this);
	// Key control
	this.cursor = game.input.keyboard.createCursorKeys();

	// Weapons
	this.weapons = [this.BP_Normal_Spawn];
	this.currentWeapon = 0;

	// Buffs
	this.nextBuffAt = Game.rnd.integerInRange(2 * 1000, 15 * 1000);
	this.shootingDelayBuff = 1;

	// Explosions
	this.exxs = [];
	// HP
	this.hpbg = Game.add.sprite(20, 20, 'hpbg');
	this.hp = Game.add.sprite(20, 20, 'hp');
	// Volume
	this.vol = 1;

	this.score = 0;
	this.scoreText = Game.add.text(780, 20);
	this.scoreText.anchor.x = 1;
	this.scoreText.fill = '#fff';
	this.scoreText.font = 'Arial Black';
	this.scoreText.fontSize = 30;

	this.ultCD = Game.add.text(780, 550);
	this.ultCD.anchor.x = 1;
	this.ultCD.fill = '#ddd';
	this.ultCD.font = 'Arial Black';
	this.ultCD.fontSize = 30;

	this.nextUlt = 0;
	this.allBuffs = [];
	// Player Animation
	this.player.animations.add('still', [0], 10, true, true);
	this.player.animations.add('left1', [1], 10, true, true);
	this.player.animations.add('left2', [2], 10, true, true);
	this.player.animations.add('left3', [3], 10, true, true);
	this.player.animations.add('left4', [4], 10, true, true);
	this.player.animations.add('left5', [5], 10, true, true);
	this.player.animations.add('right1', [6], 10, true, true);
	this.player.animations.add('right2', [7], 10, true, true);
	this.player.animations.add('right3', [8], 10, true, true);
	this.player.animations.add('right4', [9], 10, true, true);
	this.player.animations.add('right5', [10], 10, true, true);
	this.player.play('still');

	game.input.onDown.add(function(pointer){
		if(Game.paused)
		{
			if(pointer.x >= 365 && pointer.x <= 365+64 && pointer.y >= 270 && pointer.y <= 270+64)
			{
				this.vol -= 0.1;
				if(this.vol < 0) this.vol = 0;
			}
			if(pointer.x >= 610 && pointer.x <= 610+64 && pointer.y >= 270 && pointer.y <= 270+64)
			{
				this.vol += 0.1;
				if(this.vol > 1) this.vol = 1;
			}
			v = this.vol * this.vol;
			this.atke.volume = v;
			this.atkp.volume = v;
			this.exp.volume = v;
		}
	}, this);
	this.specialWeapon = 0;
	this.doCollision = true;
}

Game.changePause = function() {
	Game.paused = !Game.paused;
	// Tween
	this.tweens.forEach(function(e) {
		if(Game.paused) e.pause();
		else e.resume();
	})
	// Physics
	this.physics.arcade.isPaused = Game.paused;

	if(Game.paused)
	{
		this.menu = game.add.sprite(0, 0, 'pause');
		this.percentage = game.add.text(485, 280);
		this.percentage.align = 'left';
		this.percentage.font = 'Arial Black';
		this.percentage.fontSize = 35;
		this.percentage.fontWeight = 'bold';
	} else {
		this.menu.destroy();
		this.percentage.destroy();
	}
}

Game.update = function() {
	this.ultCD.text = this.nextUlt < this.nowTime ? "ULT: CTRL" : `ULT CD: ${Math.round((-this.nowTime + this.nextUlt) / 100) / 10}`;
	this.ultCD.fill = this.nextUlt < this.nowTime ? "#fff799" : "#555";
	this.scoreText.text = `Score: ${this.score}`;
	this.hp.scale.x = this.player.hp / 100;

	if(!Game.paused)
	{
		this.nowTime += 100/6.0;
		this.PlayerUpdate(this);
		this.SpawnMonter(this);
		this.EnemyBehaviour(this);
		this.CollisionEvent();
		this.GenerateBuffs(this);

		// Bullet Follow
		if(this.bulletFollow > 0)
		{
			
			this.playerBullet.forEach(function(e) {
				if(e.alive)
				{
					
					if(e.toFollow == null || e.toFollow.alive == false){
						if(Game.enemy.getFirstAlive() == null){
							e.body.velocity.x = 0;
							e.body.velocity.y = -400;
							return;
						}
						while(e.toFollow == null || e.toFollow.alive == false) e.toFollow = Game.enemy.getRandomExists();
					}

					mag = Math.sqrt((e.toFollow.x - e.x) * (e.toFollow.x - e.x) + (e.toFollow.y - e.y) * (e.toFollow.y - e.y));
					e.body.velocity.x = (e.toFollow.x - e.x) * 400 / mag;
					e.body.velocity.y = (e.toFollow.y - e.y) * 400 / mag;
					
				}
			});
		} else {

		}

	}
	else {
		this.percentage.text = `${Math.round(this.vol*100)}`;
	}

	this.stage = Math.floor(this.nowTime / 12000);
	// Change Scroll Speed & Spawn Time
	this.scroll = scrollSpeedBase[selectedStage] + scrollSpeedGrow[selectedStage] * this.stage;
	if(this.scroll > scrollSpeedMax[selectedStage]) this.scroll = scrollSpeedMax[selectedStage];

	this.spawnTime = spawnTimeBase[selectedStage] - spawnTimeDecay[selectedStage] * this.stage;
	if(this.spawnTime < spawnTimeMin[selectedStage]) this.spawnTime = spawnTimeMin[selectedStage];
}

Game.GenerateBuffs = function(ref) {
	if(ref.nextBuffAt > ref.nowTime) return;
	console.log("Generate a Buff");
	ref.nextBuffAt = ref.nowTime + Game.rnd.integerInRange(5 * 1000, 15 * 1000);
	// Generate One of the 3 Buffs
	b = Game.rnd.integerInRange(1, 3);
	do{
		x = Game.rnd.frac() * (800-64);
		y = Game.rnd.frac() * (600-64);
	} while(Math.sqrt((x - ref.player.x) * (x - ref.player.x) + (y - ref.player.y) * (y - ref.player.y)) < 100)
	
	buff = Game.add.sprite(x, y, `buff${b}`);
	Game.physics.arcade.enable(buff);
	ref.allBuffs.push(buff);
	ref.time.events.add(4000, function(){
		Game.allBuffs[0].destroy();
		Game.allBuffs.shift();
	}, null);
}

Game.SpawnMonter = function(ref)
{
	function GenerateMonster1(ref) {
		e = ref.enemy.getFirstExists(false, false, 0, 0, 'e_crow');
		e.hp = 10;
		e.nextShootAt = 0;
		e.shootingDelay = 2000;
		e.phase = Game.rnd.frac() * Math.PI;
		e.coef = Game.rnd.frac() * 0.3 + 0.1;
		e.pos = Game.rnd.integerInRange(100, 700);
		e.range = Game.rnd.integerInRange(10, 50 * Math.exp(-(e.pos-400)*(e.pos-400)/40000) + 50);
		e.reset(e.pos, -10);
		e.body.setSize(27, 20, 0, 0);
		e.play('fly');
	}
	function GenerateMonster2(ref) {
		e = ref.enemy.getFirstExists(false, false, 0, 0, 'e_bug');
		e.hp = 30;
		e.type = 'bullet';
		e.nextShootAt = 0;
		e.shootingDelay = 500;
		e.phase = Game.rnd.frac() * Math.PI;
		e.coef = Game.rnd.frac() * 1.2 + 0.1;
		e.pos = Game.rnd.integerInRange(300, 500);
		e.range = Game.rnd.integerInRange(100, 150 * Math.exp(-(e.pos-400)*(e.pos-400)/40000) + 150);
		e.reset(e.pos, -10)
		e.body.setSize(80, 64, 0, 0);
		e.play('fly');
	}
	function GenerateMonster3(ref) {
		e = ref.enemy.getFirstExists(false, false, 0, 0, 'e_dragon');
		e.hp = 120;
		e.type = 'bullet';
		e.nextShootAt = 0;
		e.shootingDelay = 2500;
		e.phase = Game.rnd.frac() * Math.PI;
		e.coef = Game.rnd.frac() * 0.2 + 0.1;
		e.pos = Game.rnd.integerInRange(350, 450);
		e.range = Game.rnd.integerInRange(100, 250 * Math.exp(-(e.pos-400)*(e.pos-400)/40000) + 100);
		e.reset(e.pos, -10)
		e.body.setSize(96, 96, 0, 0);
		e.play('fly');
	}

	if(ref.nowTime < ref.nextEnemyAt) return;
	ref.nextEnemyAt = ref.nowTime + this.spawnTime;
	// Spawn Enemies according to number and type prob
	num = Game.rnd.integerInRange(monsterNumberMinMax[selectedStage][0], monsterNumberMinMax[selectedStage][1]);
	for(n = 0; n < num; n++) {
		prob = Game.rnd.frac();
		if(prob < monsterTypeProb[selectedStage][0]) GenerateMonster1(ref);
		if(prob < monsterTypeProb[selectedStage][1]) GenerateMonster2(ref);
		else                                         GenerateMonster3(ref);
	}
}

Game.render = function() {
	game.debug.body(this.enemy);
}

Game.CollisionEvent = function() {
	this.doCollision = !this.doCollision;
	if(this.doCollision) return;
	// Bullet Collision
	game.physics.arcade.overlap(this.enemy, this.playerBullet, this.EnemyHit, null, this);
	game.physics.arcade.overlap(this.enemyBullet, this.player, this.PlayerHit, null, this);
	game.physics.arcade.overlap(this.enemyBullet, this.playerBullet, this.BulletHit, null, this);
	game.physics.arcade.overlap(this.enemy, this.player, this.EnemyPlayerBump, null, this);
	// Buff Collision
	for(b = 0; b < Game.allBuffs.length; b++){
		game.physics.arcade.overlap(Game.player, Game.allBuffs[b], function(){
			if(Game.allBuffs[b].key == 'buff1') Game.player.hp = Math.min(100, Game.player.hp + 40);
			if(Game.allBuffs[b].key == 'buff2') 
			{
				Game.bulletFollow += 1;
				Game.time.events.add(5000, function(){
					Game.bulletFollow -= 1;
				}, null);
			}
			if(Game.allBuffs[b].key == 'buff3') 
			{
				Game.specialWeapon += 1;
				Game.time.events.add(5000, function(){
					Game.specialWeapon -= 1;
				}, null);
			}
			Game.allBuffs[b].destroy();

		}, null);
	}
}

Game.EnemyPlayerBump = function(p, e) {
	if(e.key == 'e_crow') p.hp = Math.max(0, p.hp - 10);
	else if(e.key == 'e_bug') p.hp = Math.max(0, p.hp - 50);
	else if(e.key == 'e_dragon') p.hp = Math.max(0, p.hp - 90);
	exx = Game.add.sprite(e.x, e.y, 'explosion');
	exx.anchor.x = exx.anchor.y = 0.5;
	exx.animations.add('do', [0, 1, 2, 4, 5, 6, 9], 15, false, true);
	exx.play('do');
	Game.exxs.push(exx);
	Game.time.events.add(500, function(){ Game.exxs[0].destroy(); Game.exxs.shift(); }, this);
	Game.exp.play();
	e.kill();

	if(p.hp <= 0) 
	{
		exx = Game.add.sprite(p.x, p.y, 'explosion');
		exx.anchor.x = exx.anchor.y = 0.5;
		exx.animations.add('do', [0, 1, 2, 4, 5, 6, 9], 15, false, true);
		exx.play('do');
		this.exxs.push(exx);
		this.exp.play();
		Game.time.events.add(500, function(){ Game.exxs[0].destroy(); Game.exxs.shift(); }, this);
		
		p.kill();

		Game.time.events.add(2000, function() {
			GameScore = Game.score;
			Game.state.start('Gameover');
		});
	}
}

Game.BulletHit = function(b, a) {
	if(a.key != 'bul_pl_s') a.kill();
	if(b.key != 'bul_e_dragon') b.kill();

	this.emitter.x = (a.x + b.x) / 2;
	this.emitter.y = (a.y + b.y) / 2;
	this.emitter.start(true, 500, null, 8);
}

Game.EnemyHit = function(enemy, bullet) {
	console.log("Enemy hit");
	if(bullet.key == 'bul_pl_s' && enemy.hitByFireBall == bullet) return;
	this.atkp.play();
	this.emitter.x = bullet.x;
	this.emitter.y = (enemy.y + bullet.y) / 2;
	this.emitter.start(true, 500, null, 8);
	enemy.hp -= bullet.key == 'bul_pl_s' ? 30 : 7.5;
	if(enemy.hp <= 0) {
		exx = Game.add.sprite(enemy.x, enemy.y, 'explosion');
		exx.anchor.x = exx.anchor.y = 0.5;
		exx.animations.add('do', [0, 1, 2, 4, 5, 6, 9], 15, false, true);
		exx.play('do');
		this.exxs.push(exx);
		Game.time.events.add(500, function(){ Game.exxs[0].destroy(); Game.exxs.shift(); }, this);
		this.exp.play();
		enemy.kill();
		Game.player.hp = Math.min(100, Game.player.hp + 1);
		
		if(enemy.key == 'e_crow') 
		{
			this.score += 100;
		}
		else if(enemy.key == 'e_bug')
		{
			this.score += 400;
		}
		else if(enemy.key == 'e_dragon')
		{
			this.score += 1500;
		}
	}
	if(bullet.key == 'bul_pl_s')
	{
		enemy.hitByFireBall = bullet;
	}
	else {
		bullet.kill();
	}
	
}

Game.PlayerHit = function(player, bullet) {
	console.log("player hit");
	this.atke.play();
	this.emitter.x = (player.x + player.x) / 2;
	this.emitter.y = (player.y + player.y) / 2;
	this.emitter.start(true, 500, null, 8);
	if(bullet.key == 'bul_e_crow') player.hp = Math.max(0, player.hp - 10);
	else if(bullet.key == 'bul_e_bug') player.hp = Math.max(0, player.hp - 2);
	else if(bullet.key == 'bul_e_dragon') player.hp = Math.max(0, player.hp - 35);
	if(player.hp <= 0) 
	{
		exx = Game.add.sprite(player.x, player.y, 'explosion');
		exx.anchor.x = exx.anchor.y = 0.5;
		exx.animations.add('do', [0, 1, 2, 4, 5, 6, 9], 15, false, true);
		exx.play('do');
		this.exxs.push(exx);
		this.exp.play();
		Game.time.events.add(500, function(){ Game.exxs[0].destroy(); Game.exxs.shift(); }, this);
		
		player.kill();

		Game.time.events.add(2000, function() {
			GameScore = Game.score;
			Game.state.start('Gameover');
		});
	}
	
	bullet.kill();
}

Game.PlayerUpdate = function(ref) {
	// Movement
	vel = {x:0, y:0};
	if(ref.cursor.left.isDown) vel.x -= ref.player.vel;
	if(ref.cursor.right.isDown) vel.x += ref.player.vel;
	if(ref.cursor.up.isDown) vel.y -= ref.player.vel;
	if(ref.cursor.down.isDown) vel.y += ref.player.vel;

	if(vel.x != 0 || vel.y != 0) 
		Game.physics.arcade.accelerationFromRotation(Math.atan2(vel.y, vel.x), 1600, ref.player.body.acceleration);
	else 
	ref.player.body.acceleration.set(0);

	// Animation
	if(Math.abs(ref.player.body.velocity.x) < 10) 
		ref.player.play('still');
	else {
		dir = ref.player.body.velocity.x < 0 ? 'right' : 'left';
		thres = Math.ceil(Math.abs(ref.player.body.velocity.x) / 60.0);
		ref.player.play(`${dir}${thres}`)
	}

	// Fire!
	if(ref.fireButton.isDown && ref.player.alive && ref.nextShootAt < ref.nowTime && ref.playerBullet.countDead() > 0) 
	{
		ref.weapons[ref.currentWeapon](ref);
	}

	if(ref.skillButton.isDown && ref.player.alive && ref.nextUlt < ref.nowTime && ref.playerBullet.countDead() > 0)
	{
		ref.Skill(ref);
	}
}

Game.EnemyBehaviour = function(ref) {
	// Crow
	ref.enemy.forEach(function(e) {
		if(e.alive)
		{
			if(e.key == 'e_crow')
			{
				// Enemy vel
				e.x = Math.sin(Game.time.now / 500 * e.coef + e.phase) * 1.0 * e.range + e.pos * 1.0;
				if(e.y < 10) e.y += 10;
				else e.y += ref.scroll;
				if(e.y > 600) e.kill();
				if(e.alive && e.nextShootAt <= ref.nowTime)
				{
					e.nextShootAt = ref.nowTime + e.shootingDelay;
					b = ref.enemyBullet.getFirstExists(false, false, e.x, e.y + 10, 'bul_e_crow');
					if(b != null)
					{
						b.reset(e.x, e.y + 10);
						b.body.setSize(5, 16, 0, 0);
						b.body.velocity.y = 250;
					}
				}
				
			}
			else if (e.key == 'e_bug'){
				e.x = Math.sin(Game.time.now / 500 * e.coef + e.phase) * 1.0 * e.range + e.pos * 1.0;
				if(e.y < 10) e.y += 10;
				else e.y += ref.scroll * 0.7;
				if(e.y > 600) e.kill();
				if(e.alive && e.nextShootAt <= ref.nowTime)
				{
					e.nextShootAt = ref.nowTime + e.shootingDelay;
					b = ref.enemyBullet.getFirstExists(false, false, e.x, e.y + 25, 'bul_e_bug');
					if(b != null)
					{
						b.reset(e.x, e.y + 10);
						b.body.setSize(8, 30, 0, 0);
						b.body.velocity.y = 250;
					}
				}
				
			}
			else {
				e.x = Math.sin(Game.time.now / 500 * e.coef + e.phase) * 1.0 * e.range + e.pos * 1.0;
				if(e.y < 10) e.y += 10;
				else e.y += ref.scroll * 0.4;
				if(e.y > 600) e.kill();
				if(e.alive && e.nextShootAt <= ref.nowTime)
				{
					e.nextShootAt = ref.nowTime + e.shootingDelay;
					b = ref.enemyBullet.getFirstExists(false, false, e.x, e.y + 40, 'bul_e_dragon');
					if(b != null)
					{
						
						b.reset(e.x, e.y + 10);
						b.body.setSize(50, 50, 0, 0);
						b.body.velocity.y = 250;
					}
				}
				
			}
		}
		
		
		
	});
}

Game.Skill = function(ref) {
	ref.nextUlt = ref.nowTime + 10000;
	function wave(waveOrder) {
		for(x = 0; x < 5; x++) {
			b = ref.playerBullet.getFirstExists(false, false, 0, 0, 'bul_pl_1');
			if(b != null)
			{
				rad = (22.5 * x + waveOrder * 10) * (Math.PI / 180);
				b.reset(ref.player.x - Math.cos(rad) * 10, ref.player.y -25 - Math.sin(rad) * 10);
				b.body.velocity.x = -Math.cos(rad) * 400;
				b.body.velocity.y = -Math.sin(rad) * 400;
			}
		}
	}

	for(t = 0; t < 9; t++)
		Game.time.events.add(t*100+1, wave, null, t);
}

Game.BP_Normal_Spawn = function(ref) {
	if(ref.specialWeapon > 0)
	{
		ref.nextShootAt = ref.nowTime + ref.bp.delay * 4 / ref.shootingDelayBuff;
		b = ref.playerBullet.getFirstExists(false, false, ref.player.x, ref.player.y - 25, 'bul_pl_s');
		b.reset(ref.player.x, ref.player.y - 25);
		b.body.setSize(70, 100, 0, 0);
		b.body.velocity.y = -400;
		b.toFollow = null;
		if(ref.bulletFollow && ref.enemy.getFirstAlive() != null)
		{
			e = null;
			while(e == null || !e.alive) e = ref.enemy.getRandomExists();
			b.toFollow = e;
		}
	}
	else {
		ref.nextShootAt = ref.nowTime + ref.bp.delay / ref.shootingDelayBuff;
		b = ref.playerBullet.getFirstExists(false, false, ref.player.x, ref.player.y - 25, 'bul_pl_1');
		b.reset(ref.player.x, ref.player.y - 25);
		b.body.setSize(15, 16, 0, 0);
		b.body.velocity.y = -400;
		b.toFollow = null;
		if(ref.bulletFollow && ref.enemy.getFirstAlive() != null)
		{
			e = null;
			while(e == null || !e.alive) e = ref.enemy.getRandomExists();
			b.toFollow = e;
		}
	}
	
}

//===================================================================