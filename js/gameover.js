Gameover = {}

Gameover.preload = function () {
    Gameover.load.image('bg', 'img/gameover.png')
}

Gameover.create = function () {
    this.bg = Gameover.add.sprite(400, 350, 'bg');
    this.bg.anchor.x = this.bg.anchor.y = 0.5;
    this.score = Gameover.add.text(400, 30);
	this.score.fill = '#fff';
	this.score.font = 'Arial Black';
	this.score.fontSize = 30;
    this.score.anchor.x = 0.5;
    this.score.align = 'center';
}

Gameover.update = function() {
    this.score.text = `Score: ${GameScore}`;
    if(this.input.activePointer.leftButton.isDown) Menu.state.start("Game");
    if(this.input.activePointer.rightButton.isDown) Menu.state.start("Menu");
}